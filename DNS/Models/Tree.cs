﻿namespace DNS.Models
{
    public class Tree
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsFolder { get; set; }
        public int? ParentId { get; set; }
        public virtual Tree Parent { get; set; }
    }
}
