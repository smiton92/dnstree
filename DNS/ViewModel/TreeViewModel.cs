﻿namespace DNS.ViewModel
{
    public class TreeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsFolder { get; set; }
        public int? ParentId { get; set; }
    }
}
