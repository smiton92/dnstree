﻿using DNS.Models;
using Microsoft.EntityFrameworkCore;

namespace DNS.Data
{
    public class TreeContext : DbContext, ITreeContext
    {
        public TreeContext(DbContextOptions<TreeContext> options)
            : base(options)
        {
        }

        public DbSet<Tree> Tree { get; set; }
    }
}
