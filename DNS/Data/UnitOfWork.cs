﻿using System;

namespace DNS.Data
{
    public class UnitOfWork : IDisposable
    {
        private readonly ITreeContext context;
        private SqlTreeRepository treeRepository;
        public UnitOfWork(ITreeContext context)
        {
            this.context = context;
        }
        public SqlTreeRepository TreeRepository
        {
            get
            {

                if (treeRepository == null)
                {
                    treeRepository = new SqlTreeRepository(context);
                }
                return treeRepository;
            }
        }
        public int Save()
        {
            return context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
