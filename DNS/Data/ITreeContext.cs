﻿using DNS.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace DNS.Data
{
    public interface ITreeContext
    {
        DbSet<Tree> Tree { get; set; }
        EntityEntry Entry(object entity);
        void Dispose();
        int SaveChanges();
    }
}
