﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DNS.Models;
using DNS.ViewModel;
using Microsoft.EntityFrameworkCore;

namespace DNS.Data
{
    public class SqlTreeRepository : ITreeRepository
    {
        private readonly ITreeContext _context;

        public SqlTreeRepository(ITreeContext context)
        {
            _context = context;
        }

        public void Create(Tree item)
        {
            _context.Tree.Add(item);
        }

        public void Delete(int id)
        {
            Tree tree = this.GetTree(id);
            if (tree != null)
                _context.Tree.Remove(tree);
        }

        public Tree GetTree(int id)
        {
            return _context.Tree.Find(id);
        }

        public IEnumerable<Tree> GetTreeList()
        {
            return _context.Tree;
        }

        public void Update(Tree item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }

        public void UpdateParentID(Tree tree, int parent_id) 
        {
            if (parent_id == 0)
            {
                tree.ParentId = null;
            }
            else
            {
                tree.Parent = GetTree(parent_id);
            }
            Update(tree);
        }
        public IEnumerable GetTreeListForView()
        {
            List<TreeViewModel> TreeListForView = GetTreeList()
                .Select(u => new TreeViewModel
                {
                    Id = u.Id,
                    Name = u.Name,
                    IsFolder = u.IsFolder,
                    ParentId = u.ParentId
                }).ToList();
            return TreeListForView;
        }
    }
}
