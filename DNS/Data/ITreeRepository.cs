﻿using DNS.Models;
using System.Collections;
using System.Collections.Generic;

namespace DNS.Data
{
    interface ITreeRepository
    {
        IEnumerable<Tree> GetTreeList();
        Tree GetTree(int id);
        void Create(Tree item);
        void Update(Tree item);
        void Delete(int id);
        void UpdateParentID(Tree tree, int parent_id);
        IEnumerable GetTreeListForView();
    }
}
