﻿function editTree(parent_id, id) {
    $.ajax({
        url: 'Tree',
        dataType: 'json',
        method: 'PUT',
        data: { 'parent_id': parent_id, 'id': id },
        beforeSend: function () {
            $('#loader').removeClass('loader')
            $('#loader').addClass('loader')
            $('#loader').removeClass('hide')
        },
        success: function (data) {
            $('#tree').jstree().redraw()
            $('#loader').addClass('hide')
        }
    });
}

function getType(data) {
    return data['isFolder'] ? 'folder' : 'file'
}

function getParent(data) {
    return data['parentId'] === null ? '#' : data['parentId']
}

function convertToJsTreeFormat(data) {
    for (variable in data) {
        data[variable]['text'] = data[variable]['name']
        data[variable]['type'] = getType(data[variable]);
        data[variable]['parent'] = getParent(data[variable])
    }
    return data
}

function createJSTrees(jsonData) {
    $('#tree')
        .jstree({
            'core': {
                'data': jsonData,
                'check_callback': true,
                'themes': {
                    'dots': false
                }
            },
            'plugins': ['dnd', 'types'],
            'types': {
                'folder': {
                    'icon': '/img/folder.png'
                },
                'file': {
                    'icon': '/img/file.png',
                    'valid_children' : [],
                }
            },
        })
        .on('move_node.jstree', function (e, node) {
            editTree(node.parent, node.node.id)
        });
}

$(function () {
    $.ajax({
        async: true,
        type: 'GET',
        url: '/Tree/All',
        dataType: 'json',
        success: function (data) {
            json_data = convertToJsTreeFormat(data)
            createJSTrees(json_data);
        },
    });
});