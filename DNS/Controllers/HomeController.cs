﻿using Microsoft.AspNetCore.Mvc;

namespace DNS.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}