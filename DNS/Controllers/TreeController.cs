﻿using System.Collections;
using System.Linq;
using DNS.Data;
using DNS.Models;
using DNS.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace DNS.Controllers
{
    public class TreeController : Controller
    {
        private readonly ITreeRepository treeRepository;
        private readonly UnitOfWork unitOfWork;

        public TreeController(ITreeContext context)
        {
            unitOfWork = new UnitOfWork(context);
            treeRepository = unitOfWork.TreeRepository;
        }

        [HttpPut]
        public bool Index(int id, int parent_id)
        {
            var Tree = treeRepository.GetTree(id);
            treeRepository.UpdateParentID(Tree, parent_id);
            return unitOfWork.Save() > 0;
        }

        public IEnumerable All()
        {
            return treeRepository.GetTreeListForView();
        }

        
    }
}