﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using DNS.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DNS
{
    public class Startup
    {
        private IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            string connectionString = Configuration.GetConnectionString("TreeContext");
            connectionString = connectionString.Replace("__PROJECT_FOLDER__", Environment.CurrentDirectory);
            services.AddDbContext<TreeContext>(options =>
                    options.UseSqlServer(connectionString));
            services.AddScoped<ITreeContext>(provider => provider.GetService<TreeContext>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
            }

            app.UseMvcWithDefaultRoute();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute("Home", "",
                         defaults: new { controller = "Home", action = "Index" });
            });
        }
    }
}
