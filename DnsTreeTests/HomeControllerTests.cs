using DNS.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Web.Mvc;

namespace DnsTreeTests
{
    [TestClass]
    public class HomeControllerTests
    {

        private TestContext testContextInstance;
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }
        [TestMethod]
        public void Idex_return_View()
        {
            var controller = new HomeController();
            var result = controller.Index();
            Assert.IsTrue(result is Microsoft.AspNetCore.Mvc.ViewResult);
        }

    }
}
