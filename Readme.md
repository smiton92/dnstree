# Запуск проекта
1. Установится dotnet следуюя [инструкции](https://dotnet.microsoft.com/download) 
2. Склонировать репозиторий ``` git clone https://smiton92@bitbucket.org/smiton92/dnstree.git```
3. Перейтив в папку DNS ``` cd dnstree/DNS```
4. Запустить проект ``` dotnet run ```